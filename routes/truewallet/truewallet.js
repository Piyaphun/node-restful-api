const express = require('express')
const router = express.Router()
const axios = require('axios')
const crypto = require('crypto')
const CryptoJS = require('crypto-js')
const { prepareData, encryPass } = require('../lib/prepare_data')
const { setTruewalletLog } = require('../lib/set_truewallet_log')

const time = Date.now()
const type = 'mobile'
const mobile = crypto.randomBytes(40)
const wordArray = CryptoJS.enc.Utf8.parse(mobile.toString())
const mobileTracking = CryptoJS.enc.Base64.stringify(wordArray)
const deviceId = CryptoJS.MD5(mobileTracking)

router.route('/getOTP').post(async (req, res) => {
  const headers = {
    timestamp: time,
    type: type,
    username: req.body.username,
    password: encryPass(req.body.username, req.body.password, time)
  }
  const urlGetOtp = `${process.env.TRUEWALLET_URL_API}${process.env.TRUEWALLET_URL_REQUEST_OTP}`
  axios
    .get(urlGetOtp, {
      headers: headers
    })
    .then((resp) => {
      res.status(200).json({ message: 'Success', data: resp.data })
    })
    .catch((handle, callback, err) => {
      res.status(400).json({ message: 'error' })
    })
})

router.route('/submitOTP').post(async (req, res) => {
  const body = prepareData(
    deviceId,
    type,
    req.body.username,
    req.body.password,
    mobileTracking,
    req.body.otp,
    req.body.ref,
    time
  )
  const headers = {
    timestamp: time,
    'X-Device': deviceId.toString().substring(16)
  }

  const urlSubmit = `${process.env.TRUEWALLET_URL_API}${process.env.TRUEWALLET_URL_SUMMIT_OTP}`
  axios
    .post(urlSubmit, body, {
      headers: headers
    })
    .then((resp) => {
      res.status(200).json({
        message: 'success',
        data: resp.data.data
      })
    })
    .catch((handle, callback, err) => {
      res.status(400).json({ message: 'error' })
    })
})

router.route('/getTransaction').post(async (req, res) => {
  const dateObj = new Date()
  const month = dateObj.getUTCMonth() + 1
  const day = dateObj.getUTCDate()
  const year = dateObj.getUTCFullYear()

  const startdate = year + '-' + '0' + month + '-' + (parseInt(day, 10) - 3)
  const enddate = year + '-' + '0' + month + '-' + (parseInt(day, 10) + 3)

  const urlTransaction = `${process.env.TRUEWALLET_URL_API}${process.env.TRUEWALLET_URL_GET_TRANSACTION}${startdate}&end_date=${enddate}&limit=50`
  const transactions = await axios.get(urlTransaction, {
    headers: {
      'X-Device': deviceId.toString().substring(16),
      Authorization: req.body.token
    }
  })
  const urlGetBalance = `${process.env.TRUEWALLET_URL_API}${process.env.TRUEWALLET_URL_GET_BALANCE}`
  const balanceAll = await axios.get(urlGetBalance, {
    headers: {
      Authorization: req.body.token
    }
  })

  let totalBalance
  if (!balanceAll.data) {
    totalBalance = 'ไม่สามารถเช็คค่าได้ ...'
  }
  totalBalance = balanceAll.data.data.current_balance

  if (transactions.data) {
    res.status(200).json({
      message: 'success',
      data: setTruewalletLog(transactions.data),
      Total_Balance: totalBalance
    })
  } else {
    res.status(400).json({
      message: 'ไม่สามารถเช็คค่าได้ ...'
    })
  }
})

module.exports = router
