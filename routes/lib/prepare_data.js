const SHA256 = require('crypto-js/sha256')

const prepareData = (
  deviceId,
  type,
  username,
  password,
  mobileTracking,
  otpCode,
  otpRef,
  time
) => {
  const data = {
    brand: 'apple',
    device_os: 'ios',
    device_name: 'chick4nnn’s iPhone',
    device_id: deviceId.toString().substring(16),
    model_number: 'iPhone 11 Pro',
    model_identifier: 'iPhone 11 Pro',
    app_version: '5.5.1',
    type: type,
    username: username,
    password: encryPass(username, password, time),
    mobile_tracking: mobileTracking,
    otp_code: otpCode,
    otp_reference: otpRef,
    timestamp: time,
    mobile_number: username
  }
  return data
}

const encryPass = (username, password, time) => {
  const a = SHA256(username + password)
  const b = SHA256(time.toString().substring(4))
  const pass = SHA256(b + a)
  return pass.toString()
}

module.exports = {
  prepareData,
  encryPass
}
