const setTruewalletLog = (data) => {
  const log = []
  data.data.activities.forEach(async (data, index) => {
    if (data.amount.indexOf('+') > -1) {
      const amount = data.amount.replace('+', '')
      let phoneNumber = data.sub_title.replace('-', '')
      phoneNumber = phoneNumber.replace('-', '')
      const dateTime = data.date_time.split(' ')
      let date = dateTime[0] + '20'
      const time = dateTime[1]
      date = date.split('/')
      date = date[2] + '-' + date[1] + '-' + date[0]
      log.push({
        datetime: date + ' ' + time,
        title: data.title + ' ' + phoneNumber,
        phone_number: phoneNumber,
        bank_number: '0886588604',
        deposit_amount: amount
      })
    }
  })
  if (log.length === 0) {
    log.push({
      data: 'No transacetion today ...'
    })
  }
  return log
}

module.exports = { setTruewalletLog }
