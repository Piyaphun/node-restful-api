const express = require('express')
const app = express()
const bodyParser = require('body-parser')
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
require('dotenv').config()

const server = app.listen(2000, function () {
  const host = server.address().address
  const port = server.address().port
  console.log('Application Run AT htttp://%s:%s', host, port)
})

const requestotp = require('./routes/truewallet/truewallet')

// route truewallet//
app.use('/api/truewallet', requestotp)
